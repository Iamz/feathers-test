/**
 * Created by Iamz on 8/24/2016.
 */

const blobService = require('feathers-blob');
const AWS = require('aws-sdk');
const S3BlobStore = require('s3-blob-store');
const multer = require('multer');
const upload = multer();
const dauria = require('dauria');

module.exports = function () {
  const app = this;

  const s3 = new AWS.S3();
  const blobStorage = S3BlobStore({
    client: s3,
    bucket: app.get('s3BucketName')
  });

  app.use('/uploads', upload.single('uri'), (req, res, next) => {
      req.feathers.file = req.file;
      next();
    },
    blobService({Model: blobStorage}));

  app.service('/uploads').before({
    create: [
      (hook) => {
        if (!hook.data.uri && hook.params.file) {
          const file = hook.params.file;
          const uri = dauria.getBase64DataURI(file.buffer, file.mimetype);
          hook.data = {uri};
        }
      },
      (hook) => {
        hook.params.s3 = { ACL: 'public-read' };
      }
    ]
  })
};
